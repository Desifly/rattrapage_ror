class Book < ApplicationRecord
  has_many :comments, dependent: :destroy
  validates :titre, presence: true,
            length: { minimum: 5 }
end
