Rails.application.routes.draw do
  get 'welcome/index'

  resources :books do
    resources :comments
  end

  root 'books#index'
end
